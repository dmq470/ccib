using System;
using System.Diagnostics;

namespace CCIB
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");
			var source = new int[4,4]
			{
				{1,0,0,0},
				{0,1,0,0},
				{0,1,0,0},
				{0,0,1,1}
			};

			var test = new ImageTransform( source);
			var final = test.Rotate();

			for(int i = 0; i<4; i++)
			{
				for(int j = 0; j<4; j++)
				{
					Console.Write( final[i,j]);
				}
				Console.WriteLine("");
			}
		}
	}
}
