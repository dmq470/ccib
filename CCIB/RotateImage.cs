using System;
using System.Diagnostics;

namespace CCIB
{
	public class ImageTransform
	{
		int[,] srcImage;
		int[,] destImage;

		public ImageTransform ( int[,] source )
		{
			srcImage = source;

			destImage = new int[4,4];
		}


		public int[,] Rotate()
		{
			for (int i = 0; i < srcImage.GetLength(0); i++)
			{
				for (int j = 0; j< srcImage.GetLength(0); j++)
				{
					destImage[i,j] = srcImage[3-j,i];

					Console.WriteLine("i: " + i + ", j: " + j + " --> " + (3 - j) + ", " + i );					//Trace.WriteLine("dest: " + dest)
				}
			}
			return destImage;
		}
	}
}

