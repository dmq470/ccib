﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedLists
{
    public class Program
    {
        static void Main(string[] args)
        {
            var list = new LinkedList();

            var arr = new[] {5, 9, 2, 25, 4, 10, 22, 14};

            var node = new Node( 15 );
            
            foreach ( var a in arr )
            {
                node.AppendToTail( a );
            }

            node.Print();

            var pivot = 9;

            Node less = null;
            Node more = null;

            Node iterator = node;
            while ( iterator.next != null )
            {
                iterator = iterator.next;
                
                if ( iterator.value < pivot )
                {
                    if ( less == null )
                        less = new Node(iterator.value);
                    else
                        less.AppendToTail( iterator.value );
                }
                else if (iterator.value > pivot)
                {
                    if (more== null)
                        more = new Node(iterator.value);
                    else
                        more.AppendToTail(iterator.value);
                }
            }

            if ( less != null ) 
                less.AppendToTail( pivot );

            Node iter = less;
            
            while ( iter.next != null )
            {
                iter = iter.next;
            }

            iter.next = more;

            Trace.WriteLine( "Testing partition" );
            
            less.Print();
        }
    }

    public class LinkedList
    {
        private Node head;
        private Node tail;

        
        public LinkedList()
        {
            head = null;
            tail = null;
        }

        public void Add( int newVal )
        {
            var newNode = new Node( newVal );
            
            if ( head == null )
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                tail.next = newNode;
                tail = tail.next;
            }
        }

        public void Print()
        {
            var iterator = head;

            while ( iterator != null )
            {
                Trace.WriteLine( iterator.value );
                
                iterator = iterator.next;
            }
        }

    }
    
    class Node
    {
        public Node next;
        public int value;

        public Node( int val )
        {
            next = null;
            value = val;
        }

        public void AppendToTail( int d )
        {
            var newNode = new Node( d );

            var item = this;

            while ( item.next != null )
            {
                item = item.next;
            }

            item.next = newNode;
        }

        public void Print()
        {
            var iterator = this;

            while (iterator != null)
            {
                Trace.WriteLine(iterator.value);

                iterator = iterator.next;
            }
        }
    }
}
